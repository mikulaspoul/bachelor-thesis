Efficient and secure document rendering from multiple similar untrusted sources
===============================================================================

Contents of this CD:

|- arca                         -- the repository with the sources of the Arca tool
  \- docs
    \- _build
      \- html                   -- the rendered documentation of Arca in HTML format
|- naucse.python.cz             -- the repository with the sources of the Naucse project
|- naucse_hooks                 -- the repository with the sources for the Naucse Hooks app
|- text_sources                 -- the repository with the source of the PDF
|- BP_Mikulas_Poul_2018.pdf     -- the PDF of the thesis text
|- README.txt                   -- this file


The repositories are included with the .git folder, so full history is available.
