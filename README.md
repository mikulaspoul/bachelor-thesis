Efficient and secure document rendering from multiple similar untrusted sources
===============================================================================

Repo for the text of my Bachelor's Thesis.

Assignments: [Official](assignment.pdf), [Naucse](https://github.com/pyvec/naucse.python.cz/issues/175)

Output: [https://bachelors-thesis.mikulaspoul.cz/](https://bachelors-thesis.mikulaspoul.cz/)

How to build
============

Install the following requirements (Fedora):

```
texlive
texlive-dirtree
texlive-arara
texlive-floatrow
texlive-xltxtra
texlive-polyglossia
texlive-minted
texlive-framed
texlive-glossaries
texlive-biblatex
texlive-biblatex-iso690
texlive-blindtext
texlive-standalone
texlive-aeguill
linux-libertine-fonts
linux-libertine-biolinum-fonts
dejavu-sans-mono-fonts
biber
graphviz
inkscape
```

And you have to download ``plantum.jar`` (http://sourceforge.net/projects/plantuml/files/plantuml.jar/download)

Alternatively, https://hub.docker.com/r/mikicz/texlive-docker/ can be used.

Run:

``` bash
make all
```
