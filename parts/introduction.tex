\emph{Nauč se Python}, Czech for \emph{Learn Python}, is a project maintained by the Czech Python community that serves as a hub for open-source teaching materials, mostly about Python.
These materials are used in courses organised by the community in the Czech Republic, for both beginners and more advanced programmers.
The main output of the project, the website naucse.python.cz, contains not only the materials themselves but also metadata about the courses, like the schedule, dates and times or the location.

The contents of materials, associated files, metadata about the courses and the rendering code are stored in a single Git (version control system) repository.
A small group of maintainers takes care of the repository, but they usually are not the primary organisers of the courses.

Only these maintainers can modify the content directly and everybody else has to use Pull Requests, a GitHub feature, to submit changes.
The maintainers have to approve any changes, resulting in the first issue with the current situation -- last-minute changes are almost impossible because the review of the changes can take even several days.

Another issue, which is only getting worse with passing time, is that any changes must be compatible with all current and past courses.
That is rather impractical when the materials need to be updated considerably or if only a specific course has to be changed.

The current situation is clearly not satisfactory.
Unfortunately, conventional solutions to this kind of problem are not ideal for this specific project and that is how this thesis came to be.

I chose this thesis topic because it combines several fields in which I am both professionally and personally interested.
The whole code is in Python, the whole project is open-source and the tools required to implement the thesis also have to be open-source.
The solution lays out quite a lot of challenges, requiring smart and creative solutions.
Most importantly, the final result will help actual people with their real problems and contribute to an excellent project helping the community provide quality teaching materials and organise great courses for the public.

This thesis will first explain in depth how the project works, the issues with the current situation and what solutions are available.
I will then analyse the assigned solution and define the requirements.

The assigned solution utilizes the functionality of Git and GitHub, the host of the base repository of the project.
GitHub allows users to make a \emph{fork} of a repository, a complete clone that belongs to them with all the privileges.
The project will use these forks as a source of parts of the website -- of specific courses.
The users will be able to make almost any change to the contents of the course in their fork, including the rendering code, but any changes will only apply to their course.

The first thing the solution requires is a tool that can render HTML fragments from Git repositories using Python.
Since the code will not be reviewed by the maintainers, the tool also must sufficiently isolate the runtime, so the main process is safe from leaks or exploits.
As the chapter about research into existing tools will reveal in more detail, there is not an existing tool suitable for the job.
Therefore, I will have to develop one.

I will write the tool using the latest Python to utilize all the latest language features and make it highly configurable.
The primary isolation provider will be Docker containers and virtual machines, however, the tool will also enable rendering HTML without isolation.
I will place great emphasis on the effectiveness of the tool, speeding up cloning repositories as much as possible, caching results to prevent duplicate work, and implementing other enhancement that will enable quick integration of this tool.

Then the tool will be integrated into the project.
In addition to allowing courses from the forks, the assignment of this thesis requires this be done efficiently, caching fragments of the content across repositories sharing the same rendering code.
The integration also has to catch any errors occurring in the forks to not break the whole site and display a warning text instead.

Finally, the changes made in the forks have to be propagated to the main website in the most automated fashion possible.
Webhooks will be used for this purpose, triggering automatic rebuilds of the website once the underlying sources change in the forks.

All the written code and all the decisions made will try to respect standard Python practices and follow the Zen of Python.
All the code will be made publicly available under open-source licenses.
While not strictly utilizing test driven development, the tests will be first-class citizens of the projects.
The individual parts of this thesis will practice the Continuous Integration principles.
