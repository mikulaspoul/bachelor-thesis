\section{Isolation}\label{sec:arca:isolation}

As mentioned in the analysis, the tool has to isolate the runtime to prevent exploits.
The isolation the tool uses is described in this section.

The research into existing tools (section~\ref{ch:research}) already rules out some ways to implement the isolation.
Sandboxing Python to only run a subset of the language is not advised (\package{RestrictedPython} in section~\ref{sec:restricted_python} and \package{pysandbox} in section~\ref{sec:pysandbox}).
Running processes in a chroot jail is not sufficient (\package{process\_isolation} in section~\ref{sec:process_isolation}).
\package{AppArmor} or \package{SELinux} could be used, but they are not even available for all Linux distributions equally, let alone on macOS or Windows.

Another detail to consider is that the tool has to be able to run on \gls{CI} tools, which limits what security measures can be used.
This rules out \package{AppArmor} and \package{SELinux} as well.

Arca provides three tiers of isolation, users can implement their own.

\subsection{No isolation}\label{subsec:arca:isolation:no_isolation}

Two backends are included with the tool which provide virtually no isolation.
They are \class{CurrentEnvironmentBackend} and \class{VenvBackend} and they both run Python in a subprocess -- they only differ in the Python used.
The first uses the same Python executable used to launch Arca, the latter uses a separate Python, from a Python virtual environment~\cite{virtual_environment} with its own requirements.

The purpose of these two backends is to provide a quick way stitch together a proof of concept -- they don't provide any actual security.
Alternatively, they can be used to programmatically run Python callables from trusted Git repositories.

\subsection{Isolation through containers}\label{subsec:arca:isolation:containers}

The \class{DockerBackend} aims to provide isolation through containers on \gls{CI} tools or in environments where isolation through virtual machines cannot be used.
It uses \emph{Docker}~\cite{docker} containers to isolate the runtime.

The approach of using Docker is much more secure than just running callables in a subprocess, however, the security is not ultimate.
As Daniel J Walsh (a security expert currently working on container integration in Red Hat~\cite{rhatdan}) says in his \articlename{Are Docker containers really secure?}~\cite{are_containers_secure} article: \directquote{Containers do not contain} and \directquote{Stop assuming that Docker and the Linux kernel protect you from malware}.

This article (and its second part \articlename{Bringing new security features to Docker}~\cite{are_containers_secure_2}) goes to great depth about the issue, but it can be summed up in the fact that the containers talk directly to the kernel.
These two articles also lay out some recommendations for running things in Docker -- which are incorporated in Arca.

The first recommendation (which is even in the \package{Docker} documentation on security~\cite{docker_security}) being followed is that \directquote{only trusted users should be allowed to control your Docker daemon}~\cite{docker_security}.
This recommendation is followed because the programmer using Arca is the \emph{trusted} user, they and only they control the containers.

The second recommendation \directquote{Don't run random Docker images on your system}~\cite{are_containers_secure} is followed by running an image made specifically for the usage by Arca.
This image is based on a stable Linux distribution, Debian.
By building a custom image for the containers, another recommendation \directquote{Drop privileges as quickly as possible}~\cite{are_containers_secure} and \directquote{Run as non-root whenever possible}~\cite{are_containers_secure_2} can be followed more easily.
The root user is only used to install system dependencies, otherwise, everything is executed under an unprivileged user which only has access to the Python executable and the data of the repository.

Docker (in its Community Edition which is required for running Arca) is available for most major Linux distributions, on macOS and on Windows~\cite{docker_install} and is available for on \product{Travis CI}~\cite{travis_ci_docker}, the \gls{CI} tool used to render Naucse.

\subsection{Isolation through virtual machines}\label{subsec:arca:isolation:vms}

Finally, the third tier of isolation is running Python in a full \gls{VM}, using \product{Vagrant}~\cite{vagrant}.
\product{Vagrant} is \directquote{a tool for building and managing virtual machine environments in a single workflow}~\cite{vagrant_intro}, which makes creating and controlling VMs easy.
\product{Vagrant} is built on top of other VM tools which makes it highly configurable and enables it to work on all platforms -- Linux, macOS and Windows are all supported~\cite{vagrant_downloads}.
The tool also works with multiple different \gls{VM} technologies (\product{Vagrant} calls them \emph{providers}), so the \gls{VM} can be launched in a variety of ways.
\product{VirtualBox}~\cite{virtualbox}, \product{VMWare}~\cite{vmware}, \product{libvirt}~\cite{libvirt} or \product{Hyper-V}~\cite{hyper_v} are available.
The VMs can be even launched on \gls{AWS}~\cite{vagrant_aws}.

The \class{VagrantBackend} uses \product{Vagrant} to create a \gls{VM} using the configured provider (\product{VirtualBox} is used by default) and then uses Docker in the \gls{VM} to run the Python callables.
This is why \class{VagrantBackend} is a subclass of \class{DockerBackend} as can be seen in figure~\ref{fig:arca_uml} -- the extra functionality is only there for launching the \gls{VM}.
Docker is used inside the \gls{VM} to serve as a further separation of the environments and to remove the need to handle Python installation inside the VM -- Docker usage is supported out of the box in \product{Vagrant}~\cite{vagrant_docker_provisioner}.

This backend is more of a proof-of-concept rather than a ready-to-use backend since the integration to Naucse did not require for it to work efficiently, but it does work and it does provide the extra layer of isolation.
