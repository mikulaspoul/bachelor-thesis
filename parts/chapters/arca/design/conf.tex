\subsection{Configuration design}\label{subsec:arca:design:conf}

The design of the configuration has two goals.
First is to make the configuration intuitive for first-time users, to make things how a Python programmer would expect them to be.
This means passing configuration options straight to constructors of the objects when creating them.
The second is overriding of the configuration with environment variables.
This makes the integration of the tool smooth on \gls{CI} tools -- the code does not have to know where it is running, it just uses the configuration provided.

These two goals are accomplished by the two ways the tool can be configured.

\subsection*{Explicit constructor options}

The first way to configure the tool is passing the values for the options to constructors of individual classes.
This way is shown in code example~\ref{lst:arca:design:conf:explicit}.

\begin{listing}[ht]
    \begin{minted}{python}
from arca import Arca, DockerBackend

Arca(base_dir="/tmp/arca",
     backend=DockerBackend(python_version="3.6.5"))
    \end{minted}
    \caption{Configuring Arca explicitly}\label{lst:arca:design:conf:explicit}
\end{listing}

\subsection*{Settings}

The second way uses a settings dictionary which also automatically takes in environment variables.
The dictionary requires keys in all capitals with a prefix -- to match the standard format of environment variables and to prevent name collisions.
The usage in shown in code example~\ref{lst:arca:design:conf:settings}.

\begin{listing}[ht]
    \begin{minted}{python}
from arca import Arca

Arca(settings={
    "ARCA_BASE_DIR": "/tmp/arca",
    "ARCA_BACKEND": "arca.DockerBackend",
    "ARCA_BACKEND_PYTHON_VERSION": "3.6.5"
})
    \end{minted}
    \caption{Configuring Arca using settings}\label{lst:arca:design:conf:settings}
\end{listing}

To further make the settings even more granular, the settings for backends have two forms.
One sets up the value for all backends which share the configuration value (with prefix \texttt{ARCA\_BACKEND}) and one sets up the value only for a specific backend (with prefix \texttt{ARCA\_<NAME>\_BACKEND}, where \texttt{<NAME>} is replaced with the name of the backend).

This dichotomy sort of goes against the \directquote{There should be one-{}-~and preferably only one~-{}-obvious way to do it} recommendation in the Zen of Python~\cite{PEP20}.
There is a \emph{single obvious} was to do it, it is just not the only one.
I chose to use a second recommendation from the Zen, \directquote{Although practicality beats purity}, because this tool will be used on \product{Travis CI}, it is really practical to enable configuring using environment variables out of the box.
