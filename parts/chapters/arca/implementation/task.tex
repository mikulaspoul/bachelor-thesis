\subsection{The Task class}\label{subsec:arca:task}

The \class{Task} class serves for defining what should be launched in the isolated environment -- what Python object should be called, where it is located in the repository, what arguments should be passed to the callable and the time limit for the execution.
This could be defined directly in arguments of the \method{run} method, but it already has quite a few different arguments.
This approach also enables the definition to be reused in multiple different repositories but defined only once and saves on some execution time by caching computed arguments in the instance.

The class is implemented to produce immutable objects -- new instance has to be created for every new task.
This is due to conversions being done in the \method{\_\_init\_\_} method and to enable easy caching of the task hash (for result caching) and of the definition \gls{JSON}\@.

The constructor has four arguments:

\begin{description}
    \tightlist
    \item[entry\_point] The string representation of the callable in the repository.
    The string representation is validated by the package \package{entrypoints}~\cite{entrypoints},
    which is inspired by the \package{setuptools} \package{entry\_points}~\cite{setuptools_entry_points}~\cite{entrypoints_docs}.

    \item[timeout] A positive integer with the time limit for the executing in seconds. The default is 5 seconds.

    \item[args] Positional arguments for the callable, strictly as a keyword argument for the constructor.
    It is optional and can be any iterable~\cite{iterable}.
    \item[kwargs] Keyword arguments for the callable, also strictly as a keyword argument for the constructor.
    It is also optional and it can be anything that is convertible to a dictionary.
\end{description}

The constructor validates the definition.
The first validation is creating an \class{EntryPoint} instance from the string representation -- \class{EntryPoint} is the main class of the package \package{entrypoints}.
The second validation is a check that the provided value is not just a module but an actual object in some module.
This is done by checking if the attribute \attribute{object\_name} of the \class{EntryPoint} instance is not \pynone -- if an object was passed in the string representation, this attribute contains its name, otherwise, it is \pyenone.
The timeout \argument{timeout} is also validated, by converting it to \texttt{int} and checking the value is positive.

Then the arguments are checked.
The positional arguments, if provided, are converted to a list.
The keyword arguments, if provided, are converted to a dictionary and that dictionary is checked if all the keys are strings -- only strings can be used for argument names.
Finally, the \gls{JSON} definition for the runner (more further) is generated to check if all arguments can be serialized.
The \gls{JSON} is not needed until much later in the process, but it doesn't make any sense to continue with the instance being invalid.

\subsubsection*{Hash}

For the purposes of caching a unique hash of the \class{Task} instance is required to serve in the hash key.
The generation of the hash is shown in code example~\ref{lst:arca:task_hash}.
This code example only includes the three properties of the \class{Task} class which are involved.
All three of these properties are cached using the \function{cached\_property} decorator from the \package{cached-property} package~\cite{cached_property}, meaning the calculation is only done once per initialization -- this saves some computing time.

The first property used is \function{serialized}.
This property returns a dictionary with all the information specifying the Task -- the callable used, the arguments and also the current version of Arca.
The dictionary is then used by \function{json} -- this property returns a string with a serialized \gls{JSON}.
Finally, the \function{hash} property returns a SHA256~\cite{sha256} hash of the \gls{JSON}.

\begin{listing}[ht]
    \inputminted{python}{code_examples/task_hash.py}
    \caption{Generating hash for \class{Task} instances} \label{lst:arca:task_hash}
\end{listing}
