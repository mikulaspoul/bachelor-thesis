\subsection{Running the callables}\label{subsec:arca:run}

The primary functionality of the \class{Arca} class is served by the \method{run} method.
This method clones or updates the target repository, checks if the result is in the cache and alternatively delegates the launch of the task to the configured backend.
A sequence diagram of calling the method is shown in figure~\ref{fig:arca_run}.

The method accepts the following arguments:

\begin{description}
    \tightlist
    \item[repo] The URL of the repository.
    \item[branch] The branch from the repository to use.
    \item[task]  The \class{Task} instance (more in section~\ref{subsec:arca:task}) with the definition of what should be called.
    \item[depth] Optional argument for speeding up cloning, more in section~\ref{subsec:arca:git}.
    \item[reference] Optional argument for speeding up cloning, more in section~\ref{subsec:arca:git}.
\end{description}

First, the \argument{repo} argument is validated, if the URL is something \class{Arca} can clone (only local repositories or repositories accessible publicly over HTTP/S) -- this is done by the \method{validate\_repo\_url} method.
Then, the \argument{depth} and \argument{reference} arguments are validated (more in section~\ref{subsec:arca:git}).

If those arguments are all valid the repository is cloned or updated, based on if the repository was cloned previously.
The \method{get\_files} method which handles it returns a tuple of two instances, a \class{Repo} instance and a \class{Path} instance.
The \class{Repo} class is from the package \package{gitpython}~\cite{gitpython} and is a Python interface for interacting with the cloned Git repository.
The \class{Path} class is from the standard Python library, the instance returned contains the path to where the repository was cloned.

Then the cache key is generated using the \class{Arca} \method{cache\_key} method.
The method is shown in code example~\ref{lst:arca:cache_key}.
The key and the reasoning behind it is described in section~\ref{subsec:arca:design:cache}, the implementation of the task hash is described in section~\ref{subsec:arca:task}).

\begin{listing}[ht]
    \begin{minted}{python}
def cache_key(self, repo: str, branch: str, task: Task, git_repo: Repo) -> str:
    """ Returns the key used for storing results in cache.
    """
    return "{repo}_{branch}_"
           "{hash}_{task}".format(repo=self.repo_id(repo),
                                  branch=branch,
                                  hash=self.current_git_hash(repo, branch, git_repo),
                                  task=task.hash)
    \end{minted}
    \caption{Generating cache key for results}\label{lst:arca:cache_key}
\end{listing}

Then the region's \method{get\_or\_create} method is used to get the result.
This method is passed a cache key and a callable which creates the result, it then does all the work.
It checks the cache, creates the value if it is not present, saves the value in the cache and returns it.
This process can be seen in figure~\ref{fig:arca_run}.

\begin{figure}[ht]
  \tmpframe{\includegraphics[width=\textwidth]{diagrams/arca_run}}
  \caption{UML sequence diagram of the \class{Arca} \method{run} method}
  \label{fig:arca_run}
\end{figure}

The value is returned encapsulated in a \class{Result} instance.
More about the \class{Result} class is in section~\ref{subsec:arca:backends:runner}.

The \method{run} method indicates errors by raising exceptions.
All the exceptions raised by \class{Arca} and its backends are subclassed from the \class{ArcaException} exception.
If cloning or pulling of the branch fails, \class{PullError} is raised.
If the callable cannot be imported or it raises an exception, \class{BuildError} is raised with the details of the problem.
If the task takes too long and exceeds the configured timeout, \class{BuildTimeoutError} is raised.
Individual backends also raise their own exceptions, which are described in the sections about them.
