\subsection{DockerBackend}\label{subsec:arca:backends:docker}

\class{DockerBackend} is a subclass of \class{BaseBackend} and utilizes \emph{Docker}~\cite{docker} \emph{containers} to provide isolation around Python.

Containers are \directquote{lightweight and portable encapsulations of an environment in which to run applications}~\cite{automate_docker}.
Containers are isolated from the operating system, however, they do share the kernel and system resources.
From the point of the application inside the container, it seems like it is running on a full-fledged computer.

Containers are started based on \emph{images}, ``templates'' for containers.
An image is a file, basically an immutable snapshot of a container, which can be distributed across computers.
Images are not modified when a container started based on it modifies something, the change only affects the container itself, not even other containers launched from the same image.

The process of running a task using this backend is approximately this:

\begin{enumerate}
    \tightlist
    \item Checking that Docker can be accessed.
    \item Determining the name of the container.
    \item Checking if the container with that name is running, starting it if it does not.
          After a container is started, the repository and the runner script is copied inside it.
    \item Copying the task definition file to the container.
    \item Launching the runner script in the container with the location of the definition file as an argument.
    \item Stopping the container, if \argument{keep\_container\_running} (more about that later) is not set.
    \item Returning the output or raising an exception if anything failed.
\end{enumerate}

The backend uses the \package{docker-py}~\cite{docker_py} package to communicate with Docker.
This package provides a Python interface for all the operations required by the backend.
The check that Docker is accessible is made through the \method{check\_docker\_access} method.
The method creates a client (an object from the package for communication) for the backend's instance and the calls its \method{ping} method, which raises an exception if Docker cannot be accessed.
Docker needs to be accessible by the user that is launching Arca, meaning they either have to be root or have sufficient permissions.

The name of the container serves a similar purpose to the cache key in Arca's \method{run} method.
To enable effective reuse of containers, the container name has to identify for what it is supposed to be used.
The name is determined by the \method{get\_container\_name} method, which creates a name based on the URL of the repository, the target branch name and the current commit.

Checking if a container with the generated name is running is done using the \method{container\_running} method, starting one using \method{start\_container}.
Before starting a container, an image must be selected.
That is done using the \method{get\_image\_for\_repo} method -- the images themselves are described further.
When an image is selected, a container is started using the client, with the correct working directory.
The client returns a \class{Container} instance from the package, which is then used for communicating with it.
Then the repository and the runner script are copied over to the container, using the \class{Container}'s \method{put\_archive} method.
The files have to be copied over in a tar archive -- the archives are created using Python's \method{tarfile} module.

The task definition \gls{JSON} is copied over to the container using the same methods.

Tasks are launched in the container using the container's \method{exec\_run} method.
Unfortunately, it does not provide a timeout option, so the limit is enforced by the Linux timeout command~\cite{timeout_command}.
The method returns a status code of the command and the output, the code can be checked if it is the 124 timeout returns (or 143 for Alpine).
The output is otherwise returned in a \class{Result} instance.

The container is stopped in a \texttt{finally} block if \argument{keep\_container\_running} is not set.
Otherwise, the used container is added to a set of containers used by the instance, so they can be stopped later by calling the \method{stop\_containers} method.

\subsubsection*{Images}

Containers can be started by this backend from two basic types of base images, either an image configured directly by the programmer or the default, custom built image made specifically for Arca.
The base image is then extended by installing extra dependencies or Python requirements.
The timeout of installing requirements is enforced, just like the timeout of tasks, by the Linux \texttt{timeout} command.

A specific image to use can be configured using the \argument{inherit\_image} option.
The backend does not check anything about this image, so the programmer has to know what they are doing.
The image has to have a Python and \texttt{pip} executable that can be launched by the default user.
Setting a specific image disables two other configuration options, \argument{python\_version} and \argument{apt\_dependencies}.
The first specifies what Python version should be used since the image is determined by the programmer, the option cannot be enforced.
The second specifies what extra dependencies should be installed since the backend cannot know which system the image is running, it cannot determine how to install dependencies.

The default image is based on Debian Stretch~\cite{debian_stretch}, in the slim version -- a cut-down version, which is smaller and does not include things not needed in containers.
In the first version of Arca, Alpine~\cite{alpine} was used to save space, however, it was replaced with Debian because Alpine does not support installing wheels~\cite{alpine_wheels}.
Wheels will be mentioned later in the section about releasing Arca, but practically it means that all python packages have to be built or compiled during installation, which takes a lot of time.

The default image is built in several stages to maximise reusability:

\begin{enumerate}
    \tightlist
    \item a base image with all the backend's dependencies installed,
    \item an image with Python installed,
    \item optionally an image with the extra dependencies (set by \argument{apt\_dependencies})
    \item and finally an image with the requirements installed.
\end{enumerate}

The \package{pyenv}~\cite{pyenv} tool is used to manage Python.
The slim version of Debian does not have a Python installed by default, plus I wanted to have precise control of what Python version is installed.
\package{pyenv} enables installing specific versions of CPython including the patch version, and even other implementations of Python, like PyPy or Anaconda.
The specific Python version used can be configured using the \argument{python\_version} option, by default the version of the Python used to launch Arca is selected.

Following recommendations mentioned in section~\ref{subsec:arca:isolation:containers}, the root user is only used to installing system dependencies, otherwise the \texttt{arca} user is used -- for installing Python, for installing requirements and for running Python.

To save time the first two stages are pre-built and pushed to a Docker \emph{registry}, from where they can be pulled by anyone.
A Docker registry is a server where images can be pushed and published for usage by other people.
The registry is available at \url{https://hub.docker.com/r/mikicz/arca/}.

The images are pushed there automatically using the \gls{CI} tool \product{Travis CI}, which is described in detail later.
On pushes to the master branch, after all tests pass,  a script is launched which lists all version of Python that are installable by \package{pyenv} and supported by Arca, checks if those versions are already pushed to the registry and builds images for those which are not.
The backend can be configured to ignore the images on the registry by the \argument{disable\_pull} option, forcing the backend to build all images locally.
This is used mostly for testing.

By default, the stages with extra dependencies installed and requirements have to be built locally.
The option \argument{use\_registry\_name} can be set to use a registry for these images, so they do not have to be rebuilt repeatedly on different computers or in different builds on \gls{CI} tools.
When an image is needed, the backend then first checks the repository if the image has already been built and pulls it.
After a new image is built, it is pushed to the registry to save for further usage.
To push to a registry, being logged in to docker is required, but that has to be handled by the programmer.

Push is disabled when the option \argument{registry\_pull\_only} is set.
This is needed for pull requests -- when running \gls{CI} tools on pull requests, authentication tokens and other secret keys are not available to prevent them leaking~\cite{travis_variables}, so login to docker is not available.
But if the images were built elsewhere, for example in the \branch{master} branch, they can still be reused in pull requests.

\subsubsection*{Configuration options}

Following is the summary of configuration options available for the backend:

\begin{description}
    \tightlist
    \item[use\_registry\_name] The registry where images should be pulled from and pushed to, \pynone by default.
    \item[registry\_pull\_only] Only allow pulls from the registry, \pyfalse by default.

    \item[keep\_container\_running] Prevents containers from being stopped right away after a task is finished, to save time on booting up containers, \pyfalse by default.
    \item[disable\_pull] Disables pulling of the default base images from the registry, \pyfalse by default.

    \item[inherit\_image] Defines what image that should be used instead of the default base image, \pynone by default.

    \item[python\_version] The specific Python version that should be used, \pynone by default, which sets the version of the Python used to launch Arca.
    \item[apt\_dependencies] A list of system dependencies that will be installed using \texttt{apt-get}, \pynone by default.

\end{description}
