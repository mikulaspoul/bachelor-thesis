\subsection{VagrantBackend}\label{subsec:arca:backends:vagrant}

\class{VagrantBackend} is a subclass of \class{DockerBackend}.
It utilizes \product{Vagrant} to launch a full Virtual Machine to isolate the environment.

The backend is a subclass of \class{DockerBackend} because Docker is used inside the VM to facilitate further separation of data and environments.
It also eradicates the need to install Python directly to the VM, meaning it does not matter what operating system is used inside the VM, as long as Docker is supported.

As stated in the section~\ref{subsec:arca:isolation:vms}, this backend is a proof-of-concept, since it was not needed in the Naucse integration.
The biggest problem to solve to make it ready is how the images with Python get to the VM\@.
The easiest solution in relation to the backend's subclassing of \class{DockerBackend} was to build the images in the current environment, push them to a registry and then pull them in the VM\@.
This obviously is quite inefficient, because to get the image to another location inside the same computer it has to make a two-way trip to the cloud and back.

The process of running a task using this backend is approximately this:

\begin{enumerate}
    \tightlist
    \item Checking that the VM is running, launching it otherwise.
    \item Checking that Docker can be accessed.
    \item Making sure that the Docker image for the certain requirements exists, building and pushing to the registry it otherwise.
    \item Determining the name of the container.
    \item Connecting to the VM, pulling the image and executing the task.
    \item Shutting down the VM if \argument{keep\_vm\_running} is not set.
    \item Deleting the whole VM if \argument{destroy} is set.
\end{enumerate}

The \package{python-vagrant}~\cite{python_vagrant} package is used to manage the VM\@.
One virtual machine is used per \class{Arca}'s \argument{base\_dir}, so practically for each project.
Vagrant uses a file called \filename{Vagrantfile} for the definition of a VM -- the backend creates this file based on the configuration, and \package{python-vagrant} handles the rest, launching the VM, stopping it and destroying it.

Vagrant enables sharing files with the VM using the option \emph{synced\_folder}~\cite{vagrant_synced}, the backend configures the VM to share two locations.
The location of the \filename{Vagrantfile}, where the runner script and task definitions are stored, and the folder containing cloned repositories, so they can be copied to the containers running inside the VM\@.

Checking that Docker can be accessed, that the image exists and determining the container name is all done using methods from \class{DockerBackend}.

The \package{fabric3}~\cite{fabric3} (a port of \package{fabric} to Python 3) package is used for communicating with the VM over \gls{SSH}.
\package{farbic3} provides an API for executing commands over \gls{SSH}, using \emph{tasks} (different from \class{Arca}'s tasks).
The \package{fabric3} task is defined in the \method{fabric\_task} property.
The task pulls the image from the registry, starts up the container if it is not already running and then executes the tasks.
Timeout is implemented in this backend using an argument of the \package{fabric3}'s function used to run the commands over \gls{SSH}.

By default, after the task is finished executing the VM is shut down.
Similarly to \class{DockerBackend}, this backend provides an option to keep the VM running to save time on booting it up before each task and adds a \method{stop\_vm} method which stops the VM\@.
There are two different ways of shutting down the VM, one being only stopping it and the other is stopping it and then deleting it completely.
By default, it is only stopped, but the backend can be configured using the option \argument{destroy} to delete it.

\subsubsection*{Configuration options}

The backend inherits options of \class{DockerBackend}, with \argument{keep\_containers\_running} being \pytrue by default.

\begin{description}
    \tightlist
    \item[box] The environment that should be used, very similiar to images in Docker.
               \texttt{ailispaw/barge}~\cite{barge} is used by default, a lightweight OS built specifically to run Docker containers.
               Any box can be used as long as it has Docker installed with version greater than or equal to 1.8, or not installed at all -- Vagrant can install it~\cite{vagrant_docker_provisioner}.
    \item[provider] The underlying technology used to start the VM, \texttt{virtualbox} by default.
    \item[quiet] Sets how verbose the logs should be, \pytrue by default.
    \item[keep\_vm\_running] As described above, should the VM be kept running until explicitly stopped? \pyfalse by default.
    \item[destroy] Should the VM be deleted once stopped? \pyfalse by default.
\end{description}
