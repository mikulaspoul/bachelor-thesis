\subsection{BaseBackend}\label{subsec:arca:backends:base}

The \class{BaseBackend} is an abstract class that defines the interface individual backends have to follow -- the \method{run} abstract method.
The class also implements handling of configuration, declares some common configuration options and implements some functions used by all the backends.

The configuration is handled by multiple methods.
The first being \method{\_\_init\_\_}.
This method takes all the keyword arguments provided and overrides the configuration options of the class, the options which are not overridden are lazily evaluated once needed.
The \method{inject\_arca} method is implemented, which is called once the backend is set in an \class{Arca} instance.
This method sets the \attribute{\_arca} attribute for the instance and calls the \method{validate\_configuration} method -- this method does nothing in this class, but can be overridden to validate configuration.

\class{BaseBackend} defines three configuration options which are common for all the backends:

\begin{description}
    \tightlist
    \item[requirements\_location] This configuration sets the location of a \filename{requirements.txt} file in the repositories.
                                  This file is used to install packages using \texttt{pip}, the Python package manager.
                                  The default is simply \filename{requirements.txt}, but it can also be set to \pynone to indicated that requirements should be ignored completely.
    \item[requirements\_timeout] This configuration sets how long the installation of requirements can maximally take, in seconds.
                                 Must be set to an integer or something that can be converted to one and the default is 5 minutes.
    \item[cwd] This configuration sets the relative path inside the repository where the tasks should be launched, the working directory.
               The default is the root of the repository.
\end{description}

Finally, three helper methods are implemented in this class, two related to the file with requirements and one to the task.

\begin{description}
    \tightlist
    \item[\texttt{get\_requirements\_file}] returns an absolute \class{Path} for the requirements file based on the configuration option \argument{requirements\_location} in relation to the argument \argument{path}.
                                   If the file does not exist inside of \argument{path} or if the configuration option is disabled, \pynone is returned instead.
    \item[\texttt{get\_requirements\_hash}] reads the file provided in its argument \argument{requirements\_file} and returns the SHA256 hash of the contents.
    \item[\texttt{serialized\_task}] returns the filename and the contents of the \gls{JSON} definition of the \class{Task} instance provided in the argument \argument{task}.
\end{description}

\subsection{BaseRunInSubprocessBackend}\label{subsec:arca:backends:base_subprocess}

The \class{BaseRunInSubprocessBackend} is an abstract subclass of the class \class{BaseBackend} which implements the \method{run} method to launch the task in a subprocess (using the Python \module{subprocess} module).
The Python used to launch the task is determined by the \method{get\_or\_create\_environment} abstract method which must be implemented in the subclasses of this class.

After the Python executable path is determined by the new abstract method, the file with the \gls{JSON} definition of the task is created using the \method{serialized\_task} method.
These two paths are then used by the \class{subprocess.Popen} class to launch the runner script as can be seen in code example~\ref{lst:arca:subprocess}.
The instance \attribute{process} is used to read the output of the subprocess, which is passed to a new \class{Result} instance.
The \class{Result} instance deserializes the output from \gls{JSON} and determines if the task was successful, it not, a \class{BuildError} exception is raised.
The working directory of the subprocess is determined by the \argument{cwd} configuration option, combined with the path where the target repository is stored.
The \method{communicate} method handles the timeout of the task, raising an exception if the execution takes too long.

\begin{listing}[ht]
    \begin{minted}{python}
process = subprocess.Popen([python_path,
                            str(self.RUNNER),
                            str(task_path.resolve())],
                           stdout=subprocess.PIPE,
                           stderr=subprocess.PIPE,
                           cwd=cwd)

out_stream, err_stream = process.communicate(timeout=task.timeout)
out_output = out_stream.decode("utf-8")

return Result(out_output)
    \end{minted}
    \caption{Launching a subprocess in \class{BaseRunInSubprocessBackend}}\label{lst:arca:subprocess}
\end{listing}
