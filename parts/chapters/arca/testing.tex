\section[Testing and CI]{Testing and Continuous Integration}\label{sec:arca:test}

One of the requirements for the tool states it has to be well tested.
Arca is tested using unit and integrations tests with the help of \package{pytest}~\cite{pytest}.
\package{pytest} is used because it makes it easy to create effective tests and provides some extra utils not included in the standard Python testing modules.

The tests are defined in the \folder{tests} folder in the base of the repository.
This directory contains multiple different files that contain individual tests.

The file \filename{conftest.py} defines \package{pytest} fixtures, resources that can be used in all other tests.
This file is loaded automatically by \package{pytest}~\cite{pytest_fixtures}.
There are two fixtures defined in the file, \function{temp\_repo\_func} which creates a temporary repository containing a file \filename{test\_file.py} for launching tasks and \function{temp\_repo\_static} containing a text file for testing of retrieving static files.
The repositories are created in a temporary directory using the Python module \texttt{tempfile} and are deleted once they are no longer needed.

The file \filename{common.py} defines constants used in tests and the contents of files in test repositories.
The rest of the files contains individual tests.
These tests are written in the form of functions which test individual parts of Arca and their integration in the whole tool.
Some of these functions are parameterized, meaning that they are launched multiple times by \package{pytest} with different arguments~\cite{pytest_param} -- to test more cases and different configuration options.

Following is a list of individual files with the description of the test they contain:

\begin{description}
    \tightlist
    \item[test\_arca\_class.py] Contains tests for functionality around cloning and pulling repositories, validation of arguments and of requesting paths for static files.
    \item[test\_backends.py] Contains parameterized tests for the basic functionality of backends (except for the Vagrant one, which is tested separately).
                             The tests check tasks are performed and that they return a correct output, that the backends can handle requirements and that the backends can handle unicode correctly and that they did not install the requirements to the current environment.
                             The test also checks that an exception is raised in case the timeout is exceeded, both for requirements and execution of tasks.
    \item[test\_cache.py] Contains tests for the initialization of cache and its functionality.
    \item[test\_current\_environment.py] Contains tests for individual strategies for installing requirements.
    \item[test\_docker.py] Contains tests for individual configuration options in the Docker backend.
    \item[test\_result.py] Contains tests of validation in \class{Result}.
    \item[test\_runner.py] Contains tests of the runner, loading definition file and execution, unicode handling.
    \item[test\_settings.py] Contains tests of the \class{Settings} class and its integration into \class{Arca}.
    \item[test\_single\_pull.py] Contains tests for the \attribute{single\_pull} configuration option and the pull efficiency in general.
    \item[test\_task.py] Contains tests of the validation in \class{Task} and its serialization.
    \item[test\_utils.py] Contains tests of the functions described in section~\ref{subsec:arca:utils}.
    \item[test\_vagrant.py] Contains tests of validation in Vagrant and the execution in the backend.
                            This backend is not included in the \filename{test\_backends.py} tests, because the tests would take too much time, a shortened version is used.
\end{description}

Arca also uses two tools to analyse code statically -- without any code execution.
The first is \package{flake8}~\cite{flake8} which checks that Arca's code is compliant with PEP8~\cite{PEP8}, the recommended style guide for writing Python code (with the max line length extended to 120 from the original 80).
The second tool used is \package{mypy}~\cite{mypy}, a static type checker for Python.
The type check is optional, it is more like an early warning system.

Arca uses \product{Travis CI} as its \gls{CI} tool, I selected it to match the tool used in Naucse.
Builds are triggered for all updates in the \branch{master} branch and for all pull requests.
The builds launch the tests including the static analysis.
All tests for the \class{VenvBackend} and \class{VagrantBackend} are skipped.
\product{Vagrant} is not supported on \product{Travis CI}~\cite{travis_vagrant} and virtual environments are broken.
The details of why virtualenvs do not work properly are described in the issue \#8589 I reported to \product{Travis CI}, available at \url{https://github.com/travis-ci/travis-ci/issues/8589}.
