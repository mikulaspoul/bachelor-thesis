\section{Releasing}\label{sec:arca:release}

There are multiple ways of distributing Python code.

One of them is simply publishing the source codes and leaving it to the people wanting to use them to handle the installation,
but this will only discourage people from using the code -- even with instructions the installation is too much of a hassle.

The recommended way~\cite{packaging_recommendations} is packaging the code using using \package{setuptools}~\cite{setuptools}.
This enables for the code to be installed in different environments using the \package{pip} or \package{easy\_install} tools.

Once the code is packaged using \package{setuptools} it can be installed to other environments by referencing the folder where it is on the computer or by referencing the Git repository where the code is stored.
Installing from a local folder is still very impractical -- distribution of the code still has to be handled, installing from a Git repository is better, but still complicated.
Repositories can change ownership or names, can be deleted or else corrupted, plus the command for installing from Git is hard to remember.

I decided to go one step further with the tool, uploading the package to \gls{PyPI}, the official package repository for Python, under the name \texttt{arca}.
By uploading the package there, it can be installed simply by running \texttt{python~-m~pip~install~arca}.

This section describes the process of packaging the tool and uploading the package to PyPI\@.

\subsection{Packaging}\label{subsec:arca:release:packaging}

The code gets packaged using \package{setuptools} by creating a file (usually called \filename{setup.py}) which contains a call to the \package{setuptools} function \function{setup}.
This function call only has to specify name, version and packages to include, but can and should contain much more, for example, metadata about the author, description of the package, the license, the dependencies of the package.

Once the \function{setup} function call is defined, the file can be executed from the command line with commands that specify what action should be performed.
Some of the relevant commands:

\begin{description}
    \tightlist
    \item[install] Installs the package to the current environment~\cite{setuptools_install}.
    \item[sdist] Creates an archive installable using the \texttt{install} command~\cite{packaging_distributon}.
    \item[bdist\_wheel] Creates an archive that can be installed just by moving the contents to the right place.
                        This archive is called a \emph{wheel} and its installation is much faster because the archive is just unpacked and no code has to be executed.
                        An extra package \package{wheel} needs to be installed for this command to work~\cite{packaging_distributon}.
\end{description}

The \texttt{sdist} and \texttt{bdist\_wheel} commands generate the archives in the \folder{dist} folder~\cite{packaging_distributon} and are then uploaded to \gls{PyPI} -- as shown further.

\subsubsection*{Arca's setup.py}

Apart from the usual options like name, author, keywords or descriptions, the Arca's \filename{setup.py} also defines some functionality which is not so common in other packages.

The first is optional requirements.
The backends \class{DockerBackend} and \class{VagrantBackend} require extra requirements (as described in the sections about them), which also have more requirements.
For people not using those backends, the extra dependencies only take up space and slow down installation -- the extra time can be considerate on slower machines or on \gls{CI} tools.
By using the configuration option \texttt{extras\_require}~\cite{setuptools_extras}, the package can define optional requirements under a specific keyword.
Arca uses this option for Docker and Vagrant backends as shown in code example~\ref{lst:arca:extras_require}.
The package can be then installed with the extras by running \texttt{pip~install~arca[docker]}.

\begin{listing}[ht]
    \begin{minted}{python}
setup(
    ...,
    extras_require={
        "docker": [
            "docker~=3.2.1",
        ],
        "vagrant": [
            "docker~=3.2.1",
            "python-vagrant",
            "fabric3",
        ],
    },
    ...
)
    \end{minted}
    \caption{Usage of extras\_require in Arca's setup.py}\label{lst:arca:extras_require}
\end{listing}

The package \package{pytest-runner}~\cite{pytest_runner} is used for launching tests.
After the package is added to \texttt{setup\_requires} a new command \texttt{pytest} is added to the \gls{CLI} of \filename{setup.py}.
The argument \texttt{tests\_require} contains all the dependencies for the tests.
To use the standard command \texttt{test} an alias is configured for the \texttt{pytest} command in \filename{setup.cfg}, the default options for \package{pytest} are defined in \filename{pytest.ini} and are described more in section~\ref{sec:arca:test}~\cite{pytest_runner_setup}.

Finally, a custom \gls{CLI} command \texttt{deploy\_docker\_bases} is defined by the configuration option \texttt{cmdclass}~\cite{cmdclass}.
The command builds and pushes Docker images to the registry, as is described in section~\ref{subsec:arca:backends:docker}.

\subsection{Uploading to PyPI}\label{subsec:arca:release:upload}

The final step is uploading the package to \gls{PyPI}.
An account is required to upload packages to \gls{PyPI}, registration is available at its main page.

Then the distribution archives are required -- they are generated by calling the commands \texttt{sdist} and \texttt{bdist\_wheel} (as described above).
To upload these archives to \gls{PyPI}, the package \package{twine}~\cite{twine} is required.
Its usage~\cite{twine} is shown in code example~\ref{lst:arca:pypi}, username and password are automatically prompted when the command is called.

\begin{listing}[ht]
    \begin{minted}{bash}
twine upload --repository-url https://upload.pypi.org/legacy/ dist/*
    \end{minted}
    \caption{Uploading packages to PyPI}\label{lst:arca:pypi}
\end{listing}

After the upload command is finished the package is available immediately on \gls{PyPI} at \url{https://pypi.org/project/arca}.
