\section{The assigned solution}\label{sec:analysis:assigned_solution}

This section will describe the solution selected by the maintainers of the project, the subject of this thesis.
The assignment is available at the beginning of this thesis and is described in detail in the upstream issue no. 175~\cite{naucse_175}.

The core of the solution is to render parts of the website (courses) from other sources than the base branch of the base Git repository, from \emph{arbitrary branches}.
Arbitrary branch is a term I will be using in the rest of the text which stands for both a branch of the base repository or a branch in a \emph{fork}.
A fork is a copy of a Git repository, that is fully owned by the person that forked it, with all privileges.

Instead of merging the full \filename{info.yml} file (as described in section~\ref{subsec:analysis:current_situation:structure}) to the repository, a file containing only a link to the arbitrary branch, consisting of an URL to the repository and a branch, will be merged.
When the \package{Flask} application will be asked to render this course, it will clone the arbitrary branch and use its contents to display the materials.

Figure~\ref{fig:forks_diagram} shows a diagram to help illustrate the principle.
The left part is a graph of arbitrary branches, with the left-most path that only goes up being the \branch{master} branch -- the base one.
The circles symbolize individual commits, the offshoots to the right are the branches.
The right part symbolizes the website.
The blue and green lines indicate what part of the website is rendered from which commit, blue indicating rendering from arbitrary branches, green from the base branch.

\begin{figure}[ht]
  \tmpframe{\includegraphics[width=0.5\textwidth]{images/forks_diagram}}
  \caption{Illustration of rendering parts of a website from arbitrary branches}
  \label{fig:forks_diagram}
\end{figure}

Once the solution is complete, all the issues with the current situation outlined in section~\ref{sec:analysis:issues} will have a solution:

\begin{description}
    \tightlist
    \item[\ref{subsec:analysis:issues:changes}] The organisers will be able to make changes to the content and its rendering as they wish in their forks and it will only affect their run, the canonical courses and content rendered from the base repository will remain unaffected.
    \item[\ref{subsec:analysis:issues:break}] Since runs will be rendered from an arbitrary branch, changes will only affect the canonical courses and will not break the runs.
    The changes then can be merged to ongoing runs, if the organisers wish so.
    \item[\ref{subsec:analysis:issues:old}] Finished runs can be moved to an arbitrary branch and if the maintainers of that repository do not merge any new changes, the contents will remain frozen.
    \item[\ref{subsec:analysis:issues:unfinished}] Runs relying on unfinished materials can be rendered from an arbitrary branch, the base branch will remain clean.
    \item[\ref{subsec:analysis:issues:time}] Changes made in runs rendered from forks will not be subject to a review from the maintainers, the organisers will be able to make changes when they want.
    The only thing subject to review will be the link to the arbitrary branch.
\end{description}

\subsection{Integration}\label{subsec:analysis:assigned_solution:integration}

The assignment and the upstream issue no. 175~\cite{naucse_175} also lay out some instructions for the integration of this solution into Naucse.

First, a tool must be found or implemented that enables rendering \gls{HTML} fragments from Git repositories.
This tool has to be written in Python, in a version that is compatible with the version used now in Naucse, meaning Python 3.5 or 3.6\@.
The tool must provide proper isolation of the runtime -- the code in forks is untrusted, it is not reviewed by the maintainers.
The tool has to be able to install requirements using \texttt{pip} from a requirements file~\cite{pip_requirements_file}.
Further, it should be documented, tested and released as open source under the terms of the \gls{MIT} license.

The tool has to be then integrated into Naucse, enabling rendering of parts of the page based just on a link to a Git repository and a branch within it.
According to the issue forks will usually share code and will most likely share a large part of the content as well, the integration has to cache individual fragments across arbitrary branches that share the same rendering code to prevent duplicate work.
Errors in rendering in forks must be handled in such a way as to not break the deployment of the entire website.
Instead, a warning should be displayed and if the page is available in the base branch, it should be displayed instead.

A part of the materials defined in Naucse are images like illustrations or diagrams and other files, like audio samples or prepared scripts.
These files are embedded or linked in the \gls{HTML} generated by freezing.
The tool has to be able to retrieve these files from arbitrary branches since the files therein can be changed as well.

When running locally, Naucse should behave as-is and no extra non-Python dependencies should be required.

Deployment of the page should not change in any way, the website should still be deployed using \product{Travis CI} to \product{GitHub Pages}.
A new build on \product{Travis CI} is only triggered when an update is pushed to the \branch{master} branch, so with the integration a new build must be triggered after each update in the forks.

The tool will be often launched on arbitrary branches that have not changed, so the tool must cache the results to prevent repetitive work.
This is different from the caching of individual fragments described above, this is caching the entire calls in the arbitrary branches based on the state of the branch.
This cache must work on \product{Travis CI} and must be persistent.
