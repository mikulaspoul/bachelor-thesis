\section{Content from arbitrary branches}\label{sec:naucse:content}

Rendering content from arbitrary branches has two major components.
The first is the ``client'' -- the Naucse instance that is rendering the whole page.
The second is the ``server'' -- the code from arbitrary branches that is launched using Arca.

Naucse was modified in such a way that courses (both canonical courses and runs) can be defined by a \filename{link.yml} file as well by \filename{info.yml}.
When Naucse encounters this file, it loads it to a different \emph{model} (a class for a specific kind of data) and uses that model differently.
While courses rendered from the same repository are loaded to a \class{Course} model, courses rendered from arbitrary branches are loaded to a \class{CourseLink} model.
This goes back to the directory structure~\ref{dir:naucse} on page~\pageref{dir:naucse} -- the \filename{link.yml} is still either in \folder{courses} or \folder{runs} under a identifier.

This is done in \class{MultipleModelDirProperty}, a new subclass of \class{DirProperty}.
While the original property could only return instances of one model class, the new subclass can return multiple models, based on the file that is located in the folders.
The original was initialized with a model and a path where to search for instances, the new subclass is initialized with a list of models and the path.
The list also determines preference -- if there are multiple definition files in the same directory, the model that is first in the list is returned.
The definition file is set by the class attribute \attribute{data\_filename}.

The new \class{CourseLink} is mostly compatible with \class{Course} in the basic attributes, like the title or description.
The difference is in the way these attributes are populated.
\class{Course} uses \class{YamlProperty}, a lazy-evaluated way to load the attributes from the file \filename{info.yml}.
\class{CourseLink} uses \class{YamlProperty} too, but only to load the repository URL and the branch name of the target arbitrary branch.
A new \class{ForkProperty} class is then used to load the attributes using Arca.

The \class{ForkProperty} creates a \class{Task} (the class from Arca) instance using the arguments it gets -- the target callable and arguments.
The task is then executed using Arca in the arbitrary branch.
The callable used to get attributes about the course is \function{course\_info} from \module{naucse.utils.forks}.
The function is present in the base repository as well, but quite confusingly, it is not executed elsewhere than in the arbitrary branches.
The repository can change the function to do something else, but let us assume it will work -- error handling will be explained later.

This was all on the ``client'' side, now about the ``server'' side.
The target arbitrary branch has to contain a \filename{info.yml} in the same exact folder where \filename{link.yml} was created on the ``client'' side.
The \function{course\_info} function from \module{naucse.utils.forks} loads the \class{Course} instance and returns a dictionary of serialized attributes of the instance which are important for the type of course -- more information is returned about runs then about canonical courses.

The \class{CourseLink}'s compatibility with \class{Course} is first utilized in lists of courses, both of canonical courses and of runs.
The courses from arbitrary branches are displayed there next to courses rendered from the base branch.

\subsection{Pages related to a specific course}\label{subsec:naucse:content:specific}

Another perk of \class{CourseLink}'s compatibility is that the routes related to courses work even for courses from arbitrary branches.

Here comes in handy the change described in section~\ref{subsec:naucse:changes:separate}.
The routes were modified in such a way that if the course is rendered from an arbitrary branch, the inside of the page is rendered in the branch, and if not, it is rendered as previously, from a template inside \folder{templates/content}.
The content, whether it was rendered locally or in a branch, is then put inside a common template.

The ``client'' side of rendering content from a branch is done using \class{CourseLink}'s methods, there is one for each type of page that can be served.
These methods create a \class{Task} instance to render that specific page requested and execute it using Arca.
The result is then returned to the route and the route fills the whole page including headers and footers with it.

The ``server'' side is managed by the \function{render} function, in the \module{naucse.utils.forks} module.
This function returns the rendered contents of the pages and some extra meta-data, like titles, licenses or the dictionary about where the source code described in section~\ref{subsec:naucse:changes:dynamic_source}.

\subsection{Gathering URLs to freeze}\label{subsec:naucse:content:gathering}

\package{elsa}, the package that is handling the process of \emph{freezing} the dynamic page into a static one, only extends another package, \package{Frozen-Flask}\cite{frozen_flask}.
This package works by hooking into the \package{Flask} mechanism for generating URLs to gather what pages should be rendered and frozen.
The freezing does not work when some of the links are not generated using the standard \package{Flask} \function{url\_for} function that returns an URL to a  certain route with some arguments -- like when those links are generated in a different process completely or when the content containing the links is retrieved from the cache.

This was solved in two parts.
The function on the ``server'' side uses the same mechanisms as \package{Frozen-Flask} does, and collects all the URLs that were generated in it.
These URLs are returned with the content of the page and metadata.
The ``client'' side then extends the mechanism for generating the URLs to freeze by also returning URLs returned from arbitrary branches.
