\section{Vectors of attack and their prevention}\label{sec:naucse:vectors}

The assignment states that the rendering of courses from arbitrary branches has to be done securely since the code in forks is unreviewed and should not be trusted.
This section describes individual vectors of attack and how those threats are prevented or reduced.

That said, some of the vectors are not completely mitigated because there is still some level of trust in the people submitting the courses from arbitrary branches -- their pull requests with the link to the branches still need to be reviewed and, at least for now, the maintainers will usually know the people submitting courses.
The solutions described in this section are implemented more to prevent accidental errors opposed to malicious attacks, with an inherent bonus of preparing work for the future, when actual malicious attacks will have to be prevented.
This section analyses even the unsolved vulnerabilities, so when a time comes when anybody will be able to submit courses, they can be solved completely.

\subsection{Cross-site scripting}\label{subsec:naucse:vectors:xss}

\gls{XSS} is a vulnerability in web applications which enables injecting JavaScript scripts to the websites~\cite{XSS}.
These scripts can then disrupt the functionality of the website.
Since the whole point of this thesis is to include \gls{HTML} fragments from untrusted sources, this is one of the obvious attacks.

This vector is prevented by only allowing certain \gls{HTML} elements and attributes.
Each piece of HTML from the arbitrary branches is parsed and checked.

The parsing and whitelist are managed by the \class{AllowedElementsParser} class, which implements \class{HTMLParser} from the standard Python library.
The class is used to parse HTML and raise errors if there are disallowed elements or attributes, or if the HTML cannot be parsed.

The whitelists (one for elements and one for attributes) is defined in this class and it was generated from the existing content of Naucse.
Main objects that are missing from the whitelists, on purpose, are the \texttt{script} tag and the event attributes like \texttt{onhover} etc., simply the elements and attributes from \gls{HTML} that can launch JavaScript.

To make sure no new elements are being introduced without being added to the whitelist, the same pieces of content that are checked from arbitrary branches are also checked when rendered from the base branch.
This way when a new element is added, the programmer will have to add the element to the whitelist.
This is important because if new elements were introduced without adding them to the whitelist, any content made from a new arbitrary branch would not pass the check.

If a disallowed object is used in content from arbitrary branches, Naucse treats the result as if the render failed and the error message is displayed instead.
If a disallowed object is used in content from the base branch, the exception is raised so the integration test of freezing the content fails.

\subsection{Disrupting the appearance of the website}\label{subsec:naucse:vectors:css}

It is important that the general appearance stays the same everywhere on the website.
It would not make sense for the header and footer and other common control elements to be different for different courses, the layout and style have to be consistent so users can navigate the page easily.
The original Naucse has a way for lessons to add custom \gls{CSS} for that specific lesson.
Further, the conversion from \package{Jupyter Notebook} to \gls{HTML} adds \texttt{style} elements and attributes to output to modify the appearance of individual elements.

The custom \gls{CSS} was only used in a couple of lessons, but the usage is very effective to make things clearer in the lesson, however, the styles are really only specific for those lessons and it would not make sense to move the styles to generic styles.
Further, it would not make sense to disable the feature for courses from forks, because the lessons do rely for the clarity on the styles and it would very counter-intuitive.
One would expect that the lessons will look completely the same when rendered from an arbitrary branch when nothing changes in the branch.

I chose to reduce the possibility of the disruption of appearance by limiting the scope of the custom styles to only the content of the lesson, leaving the overall appearance of the website the same.
Both of the checks are also performed on content from the base branch, same as the checks for elements and attributes above.

\subsubsection*{Lesson CSS}

The part of the website that contains lesson was placed in a \texttt{div} element with the class \texttt{lesson-content}.
This element was styled to be positioned relatively so if any elements of the lesson are positioned absolutely, they are only absolute inside that element.

The \gls{CSS} defined for the lesson is parsed and validated.
This is done using the \package{cssutils} package~\cite{cssutils}.
All the individual selectors are then prefixed with '\texttt{.lesson-content }' (without apostrophes, space intentional) which makes the selectors only apply to elements inside the element.
The snippet doing the prefixing is showing in code example~\ref{lst:naucse:prefixing}.

\begin{listing}[ht]
    \begin{minted}{python}
@staticmethod
def limit_css_to_lesson_content(css):
    parser = cssutils.CSSParser(raiseExceptions=True)
    parsed = parser.parseString(css)

    for rule in parsed.cssRules:
        for selector in rule.selectorList:
            # the space is important - there's a difference between for example
            # ``.lesson-content:hover`` and ``.lesson-content :hover``
            selector.selectorText = ".lesson-content " + selector.selectorText

    return parsed.cssText.decode("utf-8")
    \end{minted}
    \caption{Prefixing custom lesson CSS}\label{lst:naucse:prefixing}
\end{listing}

The space is very important because it ensures that the styles are only applied to elements inside the \texttt{lesson-content} element.
Two operators that would be problematic are \verb|+| and \verb|~|.
They select either the next or previous element, but for them to be used they would have to be at the start of selectors defined by the lesson (like in code example~\ref{lst:naucse:invalid_css}), but that would not pass the validation by \package{cssutils}.

\begin{listing}[ht]
    \begin{minted}{css}
+ div {
    color: red;
}
    \end{minted}
    \caption{Invalid CSS starting with +}\label{lst:naucse:invalid_css}
\end{listing}

\subsubsection*{Style elements and attributes}

The whitelist of allowed elements used in \class{AllowedElementsParser} includes \texttt{style}, however, a further check is made for those elements.
The element is only allowed if all the individual selectors start with '\texttt{.dataframe }' -- also by \package{cssutils}.
The dataframe element is generated by \package{Jupyter Notebook} for the outputs of individual cells.
By checking that there is a space after the class of the element, it is ensured that only elements inside the dataframe or at most, the previous and next elements, are modified.

Styling applied by the \texttt{style} attribute of elements only applies to the element itself, meaning the attribute is allowed in the whitelist of attributes managed by \class{AllowedElementsParser}.

\subsubsection*{Problems with the solution}

This is the first on the unresolved vulnerabilities -- it is not sufficiently secure.
I was originally operating under the assumption that \gls{CSS} is ``only styling'', but that is not the case.
As was shown by Max Chehab, malicious \gls{CSS} can be used for example for creating a keylogger~\cite{css_keylogger}.

So, even though I tried to limit the scope of the \gls{CSS} provided by arbitrary branches, this solution cannot be considered safe.
But after a consultation with the maintainers of Naucse, I did not extend the solution for managing \gls{CSS} any further -- one possible extension would be whitelisting specific rules to provide a safe subset.
The solution is not safe against malicious and untrusted input, but it does provide at least some basic protection.

\subsection{Code injection}\label{subsec:naucse:vectors:remote}

Code injection is a vulnerability that enables attackers to execute any arbitrary code in the application~\cite{code_injection}.
Since the point of this thesis is to allow exactly that, the execution of that code has to be very careful.

Naucse relies on Arca to handle this vulnerability.
Docker is used on \product{Travis CI}, separating the environment and putting time limits on the execution.
Locally, courses from arbitrary branches are disabled by default and the process of enabling them is not documented in the public-facing meta-course, meaning only someone who knows what they are doing can enable them and it is assumed they will know what they are doing.

If the environment was not properly isolated the following could happen:

\begin{itemize}
    \tightlist
    \item The secret variables defined for Travis could be revealed. Currently, there are two of them: \texttt{GITHUB\_TOKEN} which serves for deploying content to \product{GitHub Pages} and \texttt{DOCKER\_HUB\_PASSWORD} which serves for pushing images to a Docker registry.
    \item The whole build on \product{Travis CI} could be disrupted, stopping build and causing a \gls{DoS}.
    \item The content deployed to \product{GitHub Pages} could be replaced with something malicious.
\end{itemize}

\subsection{Cache poisoning}\label{subsec:nacuse:vectors:cache_poisoning}

Cache poisoning is a vulnerability resulting in an attacker being able to modify content not belonging to them by attacking the cache~\cite{cache_poisoning}.

There are two levels of caching in this thesis that could be affected by this vulnerability.

\subsubsection*{Arca cache}

Arca can cache entire results of tasks (described in section~\ref{subsec:arca:run}) and the Naucse integration does use this feature.
If the attacker could make Arca return a value for a different repository using cache, that would be cache poisoning.

However, that should be impossible because of the way Arca implements this cache.
The cache is entirely in control of Arca, which retrieves and saves the values.

The key for the cache is strictly namespaced for specific repository (by using a SHA256 hash of the repository URL).
Then the branch name and the commit hash of the last state of a repository is included in the cache.
Finally, another SHA256 hash is used to specify for what specific task the result is stored under the key.
The method creating the key can be seen in code example~\ref{lst:arca:cache_key} on page~\pageref{lst:arca:cache_key}.

The security of this key relies on the SHA265 hashing algorithm and will be broken only if the attacker is able to find a conflict using this algorithm applicable to this key.

\subsubsection*{Naucse cache}

The second cache that could be affected is the cache of individual lesson fragments.
By poisoning cache, an attacker could possibly deliver malicious content to pages outside their courses.

This problem is solved by namespacing the cache key, so the fragments only apply to their courses or, as was permitted in the \#175 issue~\cite{naucse_175}, to courses that share the exact same rendering code.
This is done by including the tree hash of the folder containing the rendering code.
The key is described in more detail in section~\ref{sec:naucse:caching}.

\subsection{Disrupting the build}\label{subsec:naucse:vectors:disrupting}

Even with isolation being handled there are ways how the arbitrary branches could disrupt the build on \product{Travis CI}.

\subsubsection*{Invalid returned values}

The arbitrary branches can be modified in any way, so the functions that are called by Naucse do not have to return what is expected of them.
If the returned values were not validated, the \package{Flask} app could fail to render some of the pages, like if keys were missing from dictionaries or \pynone was in a variable that is not handled to enable \pyenone.

So before the values returned by tasks from arbitrary branches are passed along to rendering in the full page, they are first checked to have a specific structure so the rendering does not fail.
Most of the checks are done using functions from the \module{naucse.utils.links} module, but occasionally some checks are made at specific locations where they are relevant.
If some of these checks fail, the entire result is treated as if the function failed and the error page is displayed otherwise.

\subsubsection*{Timeouting the build}

\product{Travis CI} has time limits for builds.
A build is killed after 10 minutes of nothing being printed and the total limit is 120 minutes~\cite{travis_timeout}.

Naucse uses Arca's timeouts to reduce the possibility of the build being killed by Travis, but the solution is not complete.
Even though installation of requirements is limited to 5 minutes and the execution of task limited to 5 seconds (the defaults in Arca), the attacker could trigger the task being killed by ensuring a lot of tasks would timeout.
The process of freezing does not print anything until everything is frozen, meaning only 120 tasks would reach the limit resulting in the build being killed.
Even if the process printed the progress, the problem would not go away.
The amount of lesson is dictated by the arbitrary branch itself, so an increase of required tasks to kill the build to 1440 would do nothing.

This is the second unresolved vulnerability.
It could be solved by creating a pool of time available for a certain branch, but after a consultation with the maintainers, I did not implement this.

