\section{Fragment caching}\label{sec:naucse:caching}

The requirements for the integration say that individual content fragments should be shared across arbitrary branches,
the assignment also says that arbitrary branches that share the identical code can also share the cache of fragments.

The fragment caching was implemented to cache the contents of lessons after being turned into \gls{HTML} from the source materials.
It does not make sense to cache other pages like the overview of the course, the session cover pages or the calendar, because they always depend on the specific course.
These other pages are cached only for arbitrary branches, by the Arca caching mechanisms, which caches whole results of calls to the \function{render} function.

The Arca's caching region is reused here for caching of the fragments -- it would not make sense to have two different caches inside one system.

\subsection{The key}\label{subsec:naucse:caching:key}

The key to the cache is generated by the \function{page\_content\_cache\_key} function from the \module{naucse.utils.routes} module, which generates a key based on the following (described in detail further):

\begin{itemize}
    \tightlist
    \item the Git tree hash of the rendering code,
    \item the specific page,
    \item the course variables,
    \item the Git tree hash of the lesson.
\end{itemize}

The Git tree hash is a hash of a directory inside a repository, including all its contents.
Meaning if two directories have the exact same contents in two repositories, even if they came to it in different ways, their tree hash is the same.
These tree hashes are retrieved by two functions in \module{naucse.utils.routes}, \function{get\_naucse\_tree\_hash} and \function{get\_lesson\_tree\_hash}, which both utilize the Arca's \function{get\_hash\_for\_file} util.

A page that should be rendered is defined by the identifier of the lesson and its specific subpage.

The course variables are mechanisms inside of Naucse which can modify the content slightly.
These variables are defined by a course and apply to all its lessons, so only courses that share variables can share content fragments.

\subsection{Value}\label{subsec:naucse:caching:value}

The values in this fragment cache are dictionaries that contain two values.
The rendered HTML fragment and a list of relative URLs used in the fragment.
The relative URLs are included, so when the fragment is used from the cache, the absolute URLs can be generated and frozen.
The links still work when Naucse is launched in the ``serve'' mode (section~\ref{subsec:analysis:current_situation:local} on page~\pageref{subsec:analysis:current_situation:local}), but when the ``freeze'' mode is used the URLs are not frozen because they were not generated using \function{url\_for}.

The relative URLs are retrieved again by the mechanism used by \package{Frozen-Flask}.
A temporary hook is inserted into the \package{Flask} app that records the requested URLs, which are then turned into relative ones by the \function{get\_relative\_url} function.

\subsection{Usage}\label{subsec:naucse:caching:usage}

The cache is used in two locations because there are two ways how a lesson can be rendered.

The first location is the \function{naucse.routes.page\_content} function which handles rendering lesson content from the current branch.
The function only uses the cache if the \argument{without\_cache} is not set (more further) and the current repository is not in a dirty state.
A dirty state of a repository is such a state where files were modified or added but not committed -- when somebody is modifying a lesson, the lessons should be rendered directly, not from cache.
If a repository is dirty is determined by the Arca's \function{is\_dirty} util.

The second location is where lesson content is requested from arbitrary branches, in the \function{naucse.routes.course\_page} function.
For lesson contents from arbitrary branches caching works in the following way:

\begin{description}
    \tightlist
    \item[Client] estimates the cache key and retrieves the value if it exists.
    \item[Client] adds the key, if the value was present in the cache, to the list of arguments sent to the \function{render} function in the arbitrary branch, indicating an \emph{offer} of previously rendered content.
    \item[Server] can reject the offer and render the content again or returns \pynone instead of the content -- metadata is returned anyway not depending on the content. \textbf{Server} always renders content when no offer is made.
    \item[Client] uses either the content returned by the server or the value from the cache if \pynone was returned.
    \item[Client] updates the value in the cache with the content used in the previous step.
\end{description}

The \function{render} function internally calls \function{naucse.routes.page\_content} as well, with the \argument{without\_cache} argument, which disables cache in that function.
Even if the arbitrary branch changed this so cache would be used, the cache would be non-permanent and would not alter the ``client'' cache -- the callables are called in a separate environment.

\subsection{The nothing frozen error}\label{subsec:naucse:caching:nothing_frozen}

There was a slight issue after this fragment caching was implemented.
When the content of lessons was retrieved entirely from cache, \package{Frozen-Flask} started raising an error \directquote{flask\_frozen.MissingURLGeneratorWarning: Nothing frozen for endpoints lesson\_static. Did you forget a URL generator?}, even though the URLs to the route for static files were correctly generated and frozen.

This is caused by the fact that when URLs are passed to the freezing function of \package{Frozen-Flask} as absolute URLs instead of the standard way the package registers them, they do not mark the endpoint as used and this error is raised.

This problem was solved by creating a generator, \function{lesson\_static\_generator} from \module{naucse.routes}, which generates the URLs in such a way \package{Frozen-Flask} marks the endpoint as used.
