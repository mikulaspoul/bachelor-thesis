\section{Changes of deployment on Travis CI}\label{sec:naucse:travis}

A couple of changes had to be made in the configuration of \product{Travis CI}.

\subsection{Docker}\label{subsec:naucse:travis:docker}

The most prominent were changes to enable Docker.
First, a transition was made from container-based environment to a sudo-enabled one.
While the former was launched inside a Docker container, the latter is launched in a full VM, meaning Docker can be used~\cite{travis_docker}.

This was done precisely because of Docker because the next change was to configure \class{DockerBackend} as the backend that should be used by Arca, by setting the environment variable \texttt{ARCA\_BACKEND} to \texttt{arca.DockerBackend}.
Courses from arbitrary branches were enabled by configuring the \texttt{FORKS\_ENABLED} environment variable.

Another thing that was configured for Docker was login, so images can be pushed to a registry using the \class{DockerBackend}'s \argument{use\_registry\_name} configuration option.
An account was created for the project on \product{Docker Hub}~\cite{docker_hub}, Docker's free and public registry.
The credentials for were added to the configuration file for \product{Travis CI}, \filename{.travis.yml}, by defining two environment variables, \texttt{DOCKER\_HUB\_USERNAME} and \texttt{DOCKER\_HUB\_PASSWORD}.
The former is added in a plain text format, with the name of the account, \texttt{naucse}.
The latter is added as a secret variable using Travis' \gls{CLI} tool~\cite{travis_cli}.

The two variables are then used in the \texttt{before\_script} part of the configuration to login to Docker, so the images can be pushed.
The secret variables are not available in builds of pull requests~\cite{travis_variables}, so the login is conditional -- it is only executed when the password variable exists, otherwise the \texttt{ARCA\_BACKEND\_REGISTRY\_PULL\_ONLY} variable is set so \class{DockerBackend} only attempts pulling from the registry and does not push.

\subsection{List of courses}\label{subsec:naucse:travis:list}

One issue with the usage of Docker is time.
Pulling or building one image can take up to several minutes, and several images might have to be required for one build if the forks start to have diverging requirements.
The combination of pulling or building multiple images can take a long time, enough to timeout the entire build.

For projects using the free version of \product{Travis CI} the limit is 10 minutes of nothing being printed~\cite{travis_timeout} and the process of freezing Naucse does not print anything until everything is done.
That means if the images were pulled or built during the freeze, the build could timeout.

Because of this, a new command was added to the Naucse's \gls{CLI} that lists all courses -- the command works even locally and can be launched using the code example~\ref{lst:naucse:list_courses}.
The command lists the identifier of each course, its title and if the course is a run, also the dates.
If the course is from an arbitrary branch, the repository URL and the branch name is added, but more importantly, since the title is printed, a container has to be launched to run the task that returns the title and therefore the image is not acquired during freezing.

\begin{listing}[ht]
    \begin{minted}{bash}
python -m naucse list_courses
    \end{minted}
    \caption{Listing all courses in Naucse} \label{lst:naucse:list_courses}
\end{listing}

The command goes one-by-one and prints the information, meaning that even if there are multiple Docker images that need to be pulled or built, as long as some build does not take over 10 minutes, the whole Travis build does not timeout.

The command is implemented by extending the \package{elsa}'s \gls{CLI} in the \filename{naucse/cli.py} file.

\subsection{Cache}\label{subsec:naucse:travis:cache}

Travis has a persistent cache between builds~\cite{travis_cache}.
Previously only installed Python packages were stored in this cache.
Arca in Naucse is configured to use the filesystem backend for cache, in the \folder{.arca/cache} folder, which was added to the persistent cache on \product{Travis CI}.
The cache is only available between builds in the same branch~\cite{travis_cache}, but anything to speed things up is good.

\subsection{Logs}\label{subsec:naucse:travis:logs}

After the website is frozen, the Naucse log is printed with the information if and how the rendering of some contents from arbitrary branches failed.
