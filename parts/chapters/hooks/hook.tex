\section{Triggering a new build}\label{sec:hooks:hook}

The hook is served by the route \function{push\_hook} which listens to the path \texttt{/hooks/push}.
This route reacts to POST requests, first verifying the incoming payload and if everything is valid, triggering a new build of Naucse on \product{Travis CI}.

\subsection{Veryfing the payload}\label{subsec:hooks:hook:payload}

The first round of verification is that the request matches what GitHub should send, at least the information required for identifying an update in a repository used in Naucse.
This includes checking that the \texttt{X-GitHub-Event} HTTP header is set.
The header can be set to many different values, but this hook is only interested in \texttt{push} and \texttt{ping} events.
\texttt{ping} events are sent to all new webhooks installed to GitHub to verify the hook exists -- the route can return a \texttt{OK} (status code 200) response right away and stop processing the request.
If the header is not set or its value is not \texttt{push} or \texttt{ping}, the route returns a \texttt{Bad Request} (status code 400) response with the description of the error.

Once the header is checked, the body of the request is converted from \gls{JSON}\@.
If the body is not valid \gls{JSON}, \texttt{Bad Request} is returned again -- it can happen if the webhook is not properly configured in GitHub or that somebody else sent the request.
Then the repository and the branch is retrieved from the body, again returning a \texttt{Bad Request} if the information is missing.

The second round of verification is that the arbitrary branch from the body is actually used in Naucse.
The hook app utilizes \package{Arca} for cloning or pulling the latest version of the production branch of the Naucse repository, and then checks all the \filename{link.yml} files.
If the repository and branch is not used, a \texttt{Bad Request} is returned, otherwise, the final check comes.

The last check is that the commit last pushed to the branch was not used previously to trigger a build through this app.
This is done by checking GitHub \gls{API} for the last commit (since the body of the request cannot be trusted) and checking against local records.
More about this in the section~\ref{subsec:hooks:hook:security}.

If all the checks and verifications pass a new build is triggered.

\subsection{Triggering a new build}\label{subsec:hooks:hook:trigger}

New builds are triggered on \product{Travis CI} using their \gls{API}\@.
There is a Python package for interacting with this \gls{API}, \package{TravisPy}~\cite{travispy}, but unfortunately, it does not support triggering builds.
Instead, the \package{requests}~\cite{requests} package is used to make the POST request.
The request can be seen in code example~\ref{lst:hooks:trigger}.
The required functionality from the trigger is so trivial that the example almost exactly matches the provided example using \package{curl} from the Travis \gls{API} documentation~\cite{travis_ci_trigger_docs}.
The \attribute{app} variable is the global \package{Flask} application -- the individual keys from the configuration are described more in section~\ref{sec:hooks:conf}.
The \attribute{repo} and \attribute{branch} are parameters of the function which contains the code and are included in the message so it's clear what triggered the build.

\begin{listing}[ht]
    \inputminted{python}{code_examples/trigger_build.py}
    \caption{Triggering a build on Travis CI}\label{lst:hooks:trigger}
\end{listing}

When builds are triggered automatically by Travis based on pushes to Git, it first checks if there are builds already running for the specific branch and stops them -- only the last commit matters.
This is not the case for builds triggered using the \gls{API}\@.
Travis can be configured to limit concurrent builds, however, the new builds are placed in a queue instead.
But from the point of Naucse, concurrent builds on the same branch are undesired.
Concurrent builds could even result in faulty behaviour -- the build time is not constant on Travis, in theory, an outdated build could finish after the latest build, overwriting the version deployed.
Because of this, the hook stops all running builds for the production branch.

The \package{TravisPy} package can be used here -- it implements listing builds and stopping them.
How the package is used can be seen in code example~\ref{lst:hooks:stopping}.
The condition makes sure only builds that would deploy the frozen static pages are stopped -- finished builds, builds of pull requests or different branches are excluded.

\begin{listing}[ht]
    \begin{minted}{python}
t = TravisPy(app.config['TRAVIS_TOKEN'])
for build in t.builds(slug=app.config["TRAVIS_REPO_SLUG"]):
    if (build.pending and
            not build.pull_request and
            build.commit.branch == app.config["NAUCSE_BRANCH"]):
        build.cancel()
    \end{minted}
    \caption{Stopping pending builds on Travis CI}\label{lst:hooks:stopping}
\end{listing}

\subsection{Security}\label{subsec:hooks:hook:security}

Since the URL for the webhook is public (it is described in the metacourse in Naucse and in the README file of the repository) the hook has to be secured against attacks.
The primary attack the hook is vulnerable to is \gls{DoS} because of the functionality that stops currently pending builds.
If an attacker was able to constantly trigger new builds and cancel pending builds, they could effectively stop deploying altogether.

GitHub provides a way of securing hooks -- if a secret key is provided with the webhook URL, it signs the payloads using \gls{HMAC}.
The route could then check the signature and refuse any requests that are not signed correctly.
The problem is that the webhooks have to be installed automatically for the method to work -- if the secret key was public so users could install the hook manually, it is no longer secret.
Additionally, the hooks would still have to include checks if the repository is used in Naucse.
In fact, this solution does not solve the problem completely, people with push permissions to one of the repositories used in Naucse can run a \gls{DoS} attack just by pushing new commits repeatedly.

I chose not to use the \gls{HMAC} signing.
Instead, I am using the checks described in section~\ref{subsec:hooks:hook:payload} to make sure the request is valid.
The condition that the repository is used in Naucse is not sufficient (but necessary, even if \gls{HMAC} was used), but the condition that a new commit was pushed before the hook was called limits the attacks in the same way \gls{HMAC} would -- to people with push permissions to a repository used in Naucse.
However, this method allows for manual hook installation.

The current solution is still venerable to attacks.
GitHub \gls{API}, which is used to checks if there was indeed a new commit pushed to a branch, implements rate limiting -- only a certain number of requests is allowed per some period of time~\cite{github_api_v3}.
An attacker could invoke the hook URL enough times to use up the limit and GitHub \gls{API} would temporarily cut off the hook app.
This time, deployment would not be stopped completely, updates pushed to the main branch of the repository would still trigger new builds, but any updates in arbitrary branches would be not.

After a consultation with the maintainers, I did not implement any precautions to prevent this, but there are several methods that could mitigate these attacks.
The first would be caching the results and using conditional requests (requests with a header that says to only return the values if they changed), which do not use up the limit~\cite{github_api_v3}.
The caching would be only effective while the number of different arbitrary branches remains low because this \gls{API} request is only made when the branch is used in Naucse.
Once the number of arbitrary branches increases, the solution would be to rate limit incoming requests.
