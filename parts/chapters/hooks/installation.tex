\section{Automatic installation}\label{sec:hooks:install}

The functional requirements for the Naucse integration say that the webhook should be installed in an automated fashion to the forks -- this section will describe how the automation was implemented in the webhook \package{Flask} app.

\subsection{Logging in using GitHub}\label{subsec:hooks:install:login}

I used two different packages to facilitate login using GitHub.
The first is \package{Flask-Session}~\cite{flask_session} which extends \package{Flask} with sessions, a key-value storage only valid for the current user.
This package is used to store authentication token from the second package, \package{GitHub-Flask}~\cite{github_flask}.
\package{GitHub-Flask} handles the OAuth2 authentication and authorization for GitHub.

The package \package{Flask-Session} adds server-side storage of session data.
The data can be stored using different interfaces, but since the hook application only needs to store the authentication token, the filesystem is sufficient.
Once the package is initialized, the global \package{Flask} variable \attribute{sessions} works like a regular dictionary, but storing the data persistently on the disk and making the data available to future requests with the same session cookie.

The \package{GitHub-Flask} package makes logins using GitHub very easy.
The usage is shown in code example~\ref{lst:hooks:login}.
Once the \class{GitHub} class is initialized, the programmer only has to call its \method{authorize} method with the required scope and the user is redirected to GitHub to approve the application.
Then the callback URL (marked by decorator \texttt{github.authorized\_handler}) is called with the access token -- the programmer can do with the token as they wish (here it's saved to a session).
Then the programmer has to tell the class how to retrieve the token from the selected storage, using decorator \texttt{github.access\_token\_getter}.
This is required later for using the \class{GitHub} instance to interact with the GitHub \gls{API}\@.

\begin{listing}[ht]
    \inputminted{python}{code_examples/github_login.py}
    \caption{Login using GitHub with \package{GitHub-Flask}}\label{lst:hooks:login}
\end{listing}

\subsection{Listing repositories}\label{subsec:hooks:install:list}

Once the user is logged in, the \function{index} route lists all the public \texttt{naucse.python.cz} repositories they have access to (one user can be in multiple GitHub organizations which can have the repository forked as well).
The appearance of the page is shown in figure~\ref{fig:hooks_install}.

\begin{figure}[ht]
  \tmpframe{\includegraphics[width=0.7\textwidth]{images/hooks_install}}
  \caption{The page with automatic webhook installation}
  \label{fig:hooks_install}
\end{figure}

The repositories are listed using the \class{GitHub} instance, by calling the code shown in example~\ref{lst:hooks:listing}.
This snippet returns all the public repositories the current user has access to (is the owner, admin of the group or a collaborator).
For this the scope \texttt{public\_repo} (as can be seen in code example~\ref{lst:hooks:login}) is required.
This scope, unfortunately, provides quite a lot of permissions (for example write permissions to code), but there is not another scope which would provide read-only permissions of all the user's repositories~\cite{github_api_scopes}.

\begin{listing}[ht]
    \begin{minted}{python}
github.get("user/repos", all_pages=True, data={"visibility": "public"})
    \end{minted}
    \caption{Listing public repositories accessible by the user}\label{lst:hooks:listing}
\end{listing}

Forks usually retain the name of the original repositories, so primarily, only repositories called \texttt{naucse.python.cz} are listed, but the user can list all the other repositories if they choose so.
The \texttt{naucse.python.cz} repositories are always listed first, ordered by the name of the owner, with the current user being shown first.
Then the other repositories are listed -- first the repositories of the user and then the rest sorted alphabetically.

Ideally, repositories with already activated hooks would be marked as such, but a separate request would be required for each repository shown.
That would slow down the loading of the page considerably since some users have a large number of repositories or have access to multiple organizations with a lot of repositories.

\subsection{Activating webhooks}\label{subsec:hooks:install:activate}

The last step is to actually install and activate the webhook.
The \texttt{write:repo\_hook} scope is required for adding new webhooks (the scope also allows to read existing webhooks).
Once the user clicks on the button to activate the webhooks the route \function{activate} is invoked with the owner of the repository and the name of the repository.
Before creating the webhook in the repository, a couple of checks are launched first:

\begin{itemize}
    \tightlist
    \item that the repository exists and the user can read it,
    \item that the existing webhooks can be read and
    \item that the webhook does not already exist in the repository.
\end{itemize}

If any of the checks fail then the user is redirected back to the page with listed repositories with a message explaining what went wrong.
Othewise the webhook is installed using the \class{GitHub} instance -- shown in code example~\ref{lst:hooks:install}.
\attribute{login} and \attribute{name} are the variables containing the owner and the name of the repository, \attribute{app} is the global \package{Flask} application.
When the call succeeds, the user is redirected back to the main page with a success message.
If the request fails (a \class{GitHubError} exception is raised), it means that the user most likely does not have write access to the repository and an error message is shown instead.

\begin{listing}[ht]
    \begin{minted}{python}
github.post(f"repos/{login}/{name}/hooks", {
                "name": "web",
                "config": {
                    "url": app.config["PUSH_HOOK"],
                    "content_type": "json"
                }
            })
    \end{minted}
    \caption{Adding a new webhook to repository}\label{lst:hooks:install}
\end{listing}
