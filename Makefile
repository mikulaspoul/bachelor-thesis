PARTS_FILES = $(shell find parts -type f -name '*.tex')
DIAGRAMS = $(shell find diagrams -type f -name '*.txt' | sed s/txt/pdf/g)
SVG_IMAGES = $(shell find images -type f -name '*.svg' | sed s/svg/pdf/g)

all: BP_Mikulas_Poul_2018.pdf cover/cover.pdf presentation/presentation.pdf

open: BP_Mikulas_Poul_2018.pdf
	gio open BP_Mikulas_Poul_2018.pdf >/dev/null 2>&1

clean_all: clean clean_presentation
	cd diagrams && ls -1  | grep -v txt | grep -v .gitignore | xargs rm -f
	cd cover && ls -1  | grep -v tex | grep -v .gitignore | xargs rm -f
	rm -f $(SVG_IMAGES)
	rm -rf cd_contents
	rm -f joined.tex

clean_presentation:
	ls -1 presentation/presentation.* | grep -v tex | xargs rm -f
	rm -rf presentation/_minted-presentation

clean:
	ls -1 BP_Mikulas_Poul_2018.* | grep -v tex | xargs rm -f
	rm -rf _minted-BP_Mikulas_Poul_2018

diagrams/%.latex: diagrams/%.txt
	java -jar plantuml.jar -tlatex $?
	mv $@ diagrams/temp
	head -n 3 diagrams/temp >> $@
	echo "\usepackage{fontspec}" >> $@
	echo "\setmainfont{Linux Biolinum O}" >> $@
	echo "\setmonofont [Scale=.80] {DejaVu Sans Mono}" >> $@
	tail -n +4 diagrams/temp >> $@
	rm diagrams/temp
	sed -i 's/None/\\texttt{None}/' $@

diagrams/%.pdf: diagrams/%.latex
	xelatex -output-directory diagrams $?

images/%.pdf: images/%.svg
	inkscape --file=$? --export-area-drawing --export-margin=5 --without-gui --export-pdf=$@

cover/cover.pdf: cover/cover.tex
	xelatex -output-directory cover cover.tex

BP_Mikulas_Poul_2018.pdf: *.tex bibliography.bib template/* images/* code_examples/* $(DIAGRAMS) $(PARTS_FILES) $(SVG_IMAGES) assignment.pdf
	arara BP_Mikulas_Poul_2018.tex

.PHONY: count
count:
	texcount *.tex $(PARTS_FILES)

find_acronyms:
	grep -hEo '[[:upper:]]{3,}' $(PARTS_FILES) | sort | uniq

undefined_acronyms:
	python scripts/undefined_acronyms.py

undefined_citations:
	python scripts/undefined_citations.py

undefined_refs:
	python scripts/undefined_refs.py

generate_single_file: scripts/generate_single_file.py BP_Mikulas_Poul_2018.pdf
	python scripts/generate_single_file.py

sort_acronyms:
	sort acronyms.tex -o acronyms.tex

presentation/presentation.pdf: presentation/presentation.tex $(SVG_IMAGES) presentation/fit.pdf presentation/naucse.pdf
	cd presentation && arara presentation.tex

# sorry, specific for my setup
cd_contents: CD_README.txt BP_Mikulas_Poul_2018.pdf
	mkdir cd_contents
	cp CD_README.txt cd_contents/README.txt
	git clone https://github.com/mikicz/arca.git cd_contents/arca
	cp -r ~/oss/arca/docs/_build/ cd_contents/arca/docs/_build/
	git clone https://github.com/pyvec/naucse.python.cz.git --reference-if-able=/home/miki/oss/naucse_python_cz cd_contents/naucse.python.cz
	git clone https://github.com/mikicz/naucse-hooks.git cd_contents/naucse_hooks
	git clone https://gitlab.com/mikulaspoul/bachelor-thesis.git cd_contents/text_sources
	cp BP_Mikulas_Poul_2018.pdf cd_contents/
