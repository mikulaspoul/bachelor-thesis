from flask_github import GitHub
from flask_session import Session
from flask import Flask, session, redirect, url_for, flash

app = Flask(__name__)
github = GitHub(app)
Session(app)

@app.route('/login')
def login():
    return github.authorize("public_repo,write:repo_hook")

@app.route('/github-callback')
@github.authorized_handler
def authorized(oauth_token):
    if oauth_token is None:
        flash("Login failed!", "error")
        return redirect(url_for('index'))

    session["github_access_token"] = oauth_token

    return redirect(url_for('index'))

@github.access_token_getter
def token_getter():
    return session.get("github_access_token")
