class Task:
    ...

    @cached_property
    def hash(self):
        return hashlib.sha256(bytes(self.json, "utf-8")).hexdigest()

    @cached_property
    def json(self):
        return json.dumps(self.serialized)

    @cached_property
    def serialized(self):
        import arca
        return {
            "version": arca.__version__,
            "entry_point": {
                "module_name": self._entry_point.module_name,
                "object_name": self._entry_point.object_name
            },
            "args": self._args,
            "kwargs": self._kwargs
        }
