requests.post(
    "https://api.travis-ci.org/repo/{}/requests".format(
        urllib.parse.quote_plus(app.config["TRAVIS_REPO_SLUG"])
    ),
    json={
        "request": {
            "branch": app.config["NAUCSE_BRANCH"],
            "message": f"Triggered by {repo}/{branch}"
        }
    },
    headers={
        "Authorization": f"token {app.config['TRAVIS_TOKEN']}",
        "Travis-API-Version": "3"
    }
)
