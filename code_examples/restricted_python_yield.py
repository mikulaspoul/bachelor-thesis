from RestrictedPython import compile_restricted_exec
function_with_yield = """
def func():
    for i in 10:
        yield i 

print(list(func()))
"""

byte_code = compile_restricted_exec(function_with_yield)

# prints: ('Line 4: Yield statements are not allowed.',)
print(byte_code.errors)
