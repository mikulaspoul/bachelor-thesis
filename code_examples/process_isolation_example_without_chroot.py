import os
from process_isolation import import_isolated

untrusted = import_isolated("untrusted")

# prints e.g.: 7646
print(os.getpid())

# prints e.g.: 7694
print(untrusted.getpid())

# prints: ['lost+found', 'tmp', 'mnt', 'boot', 'srv', 'usr', 'opt', 'sbin', 'etc',
#          'dev', 'root', 'run', 'proc', 'media', 'sys', 'home', 'var', 'lib',
#          'lib64', 'bin']
print(untrusted.ls_root())
