import os
import sys
import process_isolation

context = process_isolation.default_context()
context.ensure_started()

try:
    # Install the chroot
    os.mkdir('/tmp/chroot_jail')
    os.mkdir('/tmp/chroot_jail/some_folder')

    context.client.call(os.chroot, '/tmp/chroot_jail')
except OSError:
    print('This script must be run with superuser priveleges or '
          'the chroot folder already exist.')
    sys.exit(1)

# the module can be now safely imported
untrusted = context.load_module('untrusted', path=['.'])
print(untrusted.ls_root())  # prints: ['some_folder']

# clean up
os.rmdir('/tmp/chroot_jail/some_folder')
os.rmdir('/tmp/chroot_jail')
