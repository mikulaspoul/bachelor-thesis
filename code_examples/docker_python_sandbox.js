let Sandbox = require('docker-python-sandbox');
const poolSize = 1;
let mySandbox = new Sandbox({poolSize});

mySandbox.initialize(err => {
  if (err) throw new Error(`Unable to initialize the sandbox: ${err}`);

  const code = `import platform
print(platform.python_version())
print("Hello, world!")`;
  const timeoutMs = 2 * 1000;

  mySandbox.run({code: code, timeoutMs: timeoutMs, v3: true}, (err, result) => {
    if (err) throw new Error(`Unable to run the code in the sandbox: ${err}`);

    console.log(result.stdout);
    console.log(result.stderr);

    process.exit()
  })
});
