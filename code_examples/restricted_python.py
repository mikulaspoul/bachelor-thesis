from RestrictedPython import compile_restricted_exec, PrintCollector

hello_world = """
import platform

print("Hello, World!")
print(platform.python_version())
"""

safe_globals = {"_print_": PrintCollector, '_getattr_': getattr}

byte_code = compile_restricted_exec(hello_world)

exec(byte_code.code, safe_globals)

# prints:
# Hello, World!
# 3.6.5
print(safe_globals["_print"]())
