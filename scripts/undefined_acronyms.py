import re
from pathlib import Path
from pprint import pprint

complete_text = ""

for file in Path("parts/").glob("**/*.tex"):
    complete_text += file.read_text()

acronyms_used_regex = re.compile(r"([A-Z]{3,})}")

acronyms_used = set()

for match in re.finditer(acronyms_used_regex, complete_text):
    acronyms_used.add(match.groups()[0])

acronyms_defined_regex = re.compile(r"\\newacronym{([A-Z]+)}")

acronyms_defined = set()

for match in re.finditer(acronyms_defined_regex, Path("acronyms.tex").read_text()):

    acronyms_defined.add(match.groups()[0])

pprint(acronyms_used - acronyms_defined)
