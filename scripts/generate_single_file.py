from pathlib import Path
import re

basic_file = Path("BP_Mikulas_Poul_2018.tex")
output_file = Path("joined.tex")

if output_file.exists():
    output_file.unlink()

output_file.write_text(basic_file.read_text())

input_regex = re.compile(r"\\input{([a-zA-Z0-9_/]+.tex)}")

while "\\input{" in output_file.read_text():
    new_output = output_file.read_text()

    for match in re.finditer(input_regex, new_output):
        print("Found {}".format(match.group()))
        new_output = new_output.replace(match.group(), "\n{}\n".format(Path(match.groups()[0]).read_text()))

    output_file.write_text(new_output)
