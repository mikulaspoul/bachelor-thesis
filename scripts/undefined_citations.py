import re
from pathlib import Path
from pprint import pprint

complete_text = ""

for file in Path("parts/").glob("**/*.tex"):
    complete_text += file.read_text()

citations_used_regex = re.compile(r"\\cite{([a-zA-Z0-9_]+)}")

citations_used = set()

for match in re.finditer(citations_used_regex, complete_text):
    citations_used.add(match.groups()[0])

citations_defined_regex = re.compile(r"@[a-z]+{([a-zA-Z0-9_]+),")


citations_defined = set()

for match in re.finditer(citations_defined_regex, Path("bibliography.bib").read_text()):

    citations_defined.add(match.groups()[0])

pprint(citations_used - citations_defined)
